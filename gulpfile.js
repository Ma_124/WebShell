#!node_modules/gulp/bin/gulp.js -f

var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var tsify = require('tsify');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var tslint = require("gulp-tslint");

var paths = {
    pages: ['src/*.html']
};

gulp.task('html', function () {
    return gulp.src(paths.pages)
        .pipe(gulp.dest('public'));
});

gulp.task('ts:lint', function() {
    return gulp.src('src/**/*.ts')
        .pipe(tslint({
            formatter: "stylish",
            configuration: "tslint.json"
        }))
        .pipe(tslint.report());
});

gulp.task('ts', function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/main.ts'],
        cache: {},
        packageCache: {}
    })
    .plugin(tsify)
    .transform('babelify', {
        presets: ['@babel/preset-env'],
        extensions: ['.ts']
    })
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('public'));
});

gulp.task('default', gulp.parallel('html', 'ts', 'ts:lint'));

