class WebShellConfig {
    prompt: string;
    line: number;

    history: string[];
    historyPos: number;

    cwd: string;
    fs: { [fn: string]: string|string[] };

    init: () => void;
}

const webshell: WebShellConfig = {
    prompt: "ma@ma124.js.org: ~$ ",
    line: 1,

    history: [],
    historyPos: 0,
    cwd: "",
    fs: { // TODO test lsDir with more complicated dir structure
        "navbar.html": "<span style='float: right'><a href='https://ma124.js.org/Blog'>&lt;<span class='xml-tag'>a</span>&gt;Blog&lt;/<span class='xml-tag'>a</span>&gt;</a><a href='https://gitlab.com/Ma_124'>&lt;<span class='xml-tag'>a</span> href=<span class='xml-string'>'gitlab.com/Ma_124'</span>&gt;&lt;<span class='xml-tag'>img</span> src=<span class='xml-string'>'tanuki.png'</span> /&gt;&lt;/<span class='xml-tag'>a</span>&gt;</a><wbr><a href='https://github.com/Ma124'>&lt;<span class='xml-tag'>a</span> href=<span class='xml-string'>'github.com/Ma124'</span>&gt;&lt;<span class='xml-tag'>img</span> src=<span class='xml-string'>'octocat.png'</span> /&gt;&lt;/<span class='xml-tag'>a</span>&gt;</a></span>",
        "fav_langs.md": "",
        "src": [],
        "src/ma_124": [],
    },

    init: initWSh,
};

const catHelpText = escapeHtml(`Usage: cat [OPTION]... [FILE]...
Concatenate FILE(s) to standard output.

With no FILE, or when FILE is -, read standard input.

  -b, --number-nonblank    number nonempty output lines, overrides -n
  -E, --show-ends          display $ at end of each line
  -n, --number             number all output lines
  -s, --squeeze-blank      suppress repeated empty output lines
  -u                       (ignored)
      --help     display this help and exit
      --version  output version information and exit

Examples:
  cat f - g  Output f's contents, then standard input, then g's contents.
  cat        Copy standard input to standard output.

GNU coreutils online help: <http://www.gnu.org/software/coreutils/>
Full documentation at: <http://www.gnu.org/software/coreutils/cat>
or available locally via: info '(coreutils) cat invocation'
`);

const catVersionText = escapeHtml(`cat (GNU coreutils) 8.28
Copyright (C) 2019 Ma_124, ma_124.js.org
License AGPLv3+: GNU AGPL version 3 or later <http://gnu.org/licenses/agpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`);

const manHelpText = escapeHtml(`Usage: man [OPTION...] [SECTION] PAGE

If [SECTION] is specified https://linux.die.net/man/<SECTION>/<PAGE> is opened.
If not it will be searched on https://linux.die.net/man.`);

const aboutHtml = `<style>.webshell-about-link { color: #ec407a; } .webshell-about-link:hover { color: #f1749e; }</style><a class="webshell-about-link" href="https://ma124.js.org/l/AGPL/~/2019">Copyright 2019 Ma_124, ma124.js.org</a>

This is a <a class="webshell-about-link" href="https://gitlab.com/Ma_124/WebShell">WebShell</a>.
<a class="webshell-about-link" href="https://ma124.js.org">Ma_124</a> designed this for his <a class="webshell-about-link" href="https://ma124.js.org">personal website</a> but feel free to embed it in your's.`;

function escapeHtml(unsafe: string): string {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

function escapeRegex(unsafe: string) {
    return unsafe.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
}


function absPath(fn: string): string {
    return (webshell.cwd + fn)
        .replace(/\/+/g, "/")
        .replace(/\/$/g, "")
        .replace(/^\//g, "");
}

function readFile(fn: string): string|number {
    fn = absPath(fn);

    const fc = webshell.fs[fn];
    if (fc === undefined) {
        console.log(fn + " (NOT FOUND)");
        return 0; // Not found
    }

    if (fc instanceof Array) {
        return 1; // Directory
    }

    if (typeof fc === "string") {
        return fc as string;
    }

    return 0;
}

function lsDir(fn: string, trailSlash = false): string[]|number {
    fn = absPath(fn);

    if (typeof webshell.fs[fn] === "string") {
        return 2; // File
    }

    const dirs: string[] = [];
    const regex = new RegExp("^" + escapeRegex(fn) + "(" + trailSlash ? "" : "/" + "[^/]*?)$");
    for (const key of Object.keys(webshell.fs)) {
        if (key.match(regex)) {
            dirs.push(key);
        }
    }

    if (dirs.length === 0) {
        return 0;  // Not found
    }

    return dirs;
}

function handleCat(args: string[]): string {
    let stdout = "";
    let optB = false;
    let optE = false;
    let optN = false;
    let optS = false;

    for (let i = 1; i < args.length; i++) {
        if (args[i].charAt(0) === "-") {
            // TODO -bE
            switch (args[i]) {
                case "-b":
                case "--number-nonblank":
                    optB = true;
                    break;
                case "-E":
                case "--show-ends":
                    optE = true;
                    break;
                case "-n":
                case "--number":
                    optN = true;
                    break;
                case "-s":
                case "--squeeze-blank":
                    optS = true;
                    break;
                case "-u":
                    break;
                case "-v":
                case "--version":
                    return catVersionText;
                case "-h":
                case "--help":
                    return catHelpText;
                default:
                    return "cat: invalid option -- '" + args[i].replace(/^-+/g, "") + "'\nTry 'cat --help' for more information.";
            }
        }
    }

    for (let i = 1; i < args.length; i++) {
        if (args[i].charAt(0) === "-") {
            continue;
        }

        let fc = readFile(args[i]);
        if (typeof fc === "number") {
            if (fc === 0) {
                return "cat: " + args[i] + ": No such file or directory";
            } else if (fc === 1) {
                return "cat: " + args[i] + ": Is a directory";
            } else {
                // WTF
                return "cat: " + args[i] + ": What a Terrible Failure";
            }
        } else {
            if (optS) {
                fc = fc.replace(/(\r?\n)+/g, "\r\n");
                fc = fc.replace(/(<br( \/)>)+/g, "<br />");
            }

            if (optE) {
                fc = fc.replace(/\n/g, "$\r\n");
            }

            if (optB || optN) {
                let l = 1;
                fc = "1 " + fc.replace(/\n/g, (ss, ...args: any[]): any => {
                    return "\n" + ++l + " ";
                });
            }

            if (optN && !optB) {
                // TODO
            }
        }

        stdout += fc;
    }

    return stdout;
}

function handleMan(args: string[]): string {
    if (args.length === 1) {
        return "What manual page do you want?";
    } else if (args.length === 2) {
        if (args[1] === "--help" || args[1] === "-h") {
            return manHelpText;
        }

        window.open("https://www.die.net/search/?q=" + args[1]);
        return "";
    } else if (args.length === 3) {
        window.open("https://linux.die.net/man/" + args[1] + "/" + args[2]);
        return "";
    } else {
        return "man: too many arguments";
    }
}

function handleLs(args: string[]): string {
    // TODO fix again (broke after lsCwd)
    let stdout = "";
    let lsCwd = true;
    for (let i = 1; i < args.length; i++) {
        if (args[i].charAt(0) === "-") {
            switch (args[i]) {
                // TODO options
            }
        } else {
            lsCwd = false;
        }
    }

    for (let i = 1; i < args.length; i++) {
        if (args[i].charAt(0) === "-") {
            continue;
        }

        const lsr = lsDir(args[i]);
        if (typeof lsr === "number") {
            if (lsr === 0) {
                return "ls: cannot access '" + args[i] + "': No such file or directory";
            } else if (lsr === 2) {
                stdout += args[i] + " ";
            } else {
                return "ls: " + args[i] + ": What a Terrible Failure";
            }
        } else {
            stdout += lsr.join(" ") + " ";
        }
    }

    if (lsCwd) {
        stdout += (lsDir(webshell.cwd, webshell.cwd.length === 0 && lsCwd) as string[]).join(" ") + " ";
    }

    return stdout;
}

function handleCmd(args: string[]): string {
    switch (args[0]) {
        case "help":
            return "Commands: about, cat, ls, man, help"; // TODO change to ls /bin
        case "cat":
            return handleCat(args);
        case "man":
            return handleMan(args);
        case "ls":
            return handleLs(args);
        case "about":
            return aboutHtml;
        default:
            return "sh: " + webshell.line + ": " + args[0] + ": not found\n";
    }
}

function historyChange(i: number, curCmd: HTMLElement) {
    webshell.historyPos += i;

    if (webshell.history.length === 0) {
        return;
    }

    if (webshell.historyPos < 0) {
        webshell.historyPos = -1;
        curCmd.innerText = "";
        return;
    } else if (webshell.historyPos >= webshell.history.length) {
        webshell.historyPos = webshell.history.length - 1;
    }

    curCmd.innerText = webshell.history[webshell.historyPos];
}

function initWSh() {
    document.onkeydown = (e: KeyboardEvent) => {
        const curCmd = document.getElementById("current-cmd");
        if (e.key.length === 1) {
            curCmd.innerText += e.key;
            e.preventDefault();
            e.stopPropagation();
        } else if (e.code === "Backspace") {
            curCmd.innerText = curCmd.innerText.substring(0, curCmd.innerText.length - 1);
            e.preventDefault();
            e.stopPropagation();
        } else if (e.code === "ArrowUp") {
            historyChange(1, curCmd);
        } else if (e.code === "ArrowDown") {
            historyChange(-1, curCmd);
        } else if (e.code === "Enter") {
            curCmd.id = "";
            let stdout = "";
            if (curCmd.innerText.length !== 0) {
                webshell.history.unshift(curCmd.innerText);
                stdout = handleCmd(curCmd.innerText.split(/[ \t\f]/g));
                webshell.historyPos = 0;
            }
            webshell.line++;

            document.getElementById("user-cmds").innerHTML += "<br /><span class='cmd-out'>";
            document.getElementById("user-cmds").innerHTML += stdout.replace(/\r?\n/g, "<br />") + "<br />";
            document.getElementById("user-cmds").innerHTML += "</span><br /><nobr>" + webshell.prompt + "<span id='current-cmd'></span></nobr>";

            window.scrollTo(0, document.body.scrollHeight);

            e.preventDefault();
            e.stopPropagation();
        }
    };
}

export default webshell;
(window as any).webshell = webshell;
